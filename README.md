# PackageTemplate

A template for new packages/libraries.

## Setup
1. Make todos in these files:
    - UnitTest/dockerfile
    - UnitTest/PROJECTNAMEUnitTest.csproj
    - Package/dockerfile
    - Package/PROJECTNAME.csproj
    - Package/Template.cs
3. Rename files that contains the projectname:
    - Package/PROJECTNAME.csproj
    - UnitTest/PROJECTNAMEUnitTest.csproj
4. Create solution: `dotnet new sln`
5. Add projects to solution: `dotnet sln add **/*.csproj`

## Last steps
1. Go to Settings -> CI/CD -> "Container Registry tag expiration policy" and enable it for the Regex "xtmp-.*"
